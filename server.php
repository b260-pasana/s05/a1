<?php

session_start();

$username = 'johnsmith@gmail.com';
$password = '1234';
$_SESSION['username'] = $username;
$_SESSION['password'] = $password;


// Validation

if (isset($_POST['username']) && isset($_POST['password'])) {
    function validate($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
    $username = validate($_POST['username']);
    $password = validate($_POST['password']);

    if (empty($username)) {
        header("Location: index.php?error=Username is required");
        exit();
    } else if(empty($password)){
        header("Location: index.php?error=Password is required");
        exit();
    } else {

        if($_POST['username'] === $_SESSION['username'] && $_POST['password'] === $_SESSION['password']) {
            header('Location: ./homepage.php');
            exit();
        } else {
            header("Location: index.php?error=Incorrect Username and Passwword");
            exit();
        }
    }

} 

function logout() {
    session_destroy();
    header('Location: ./index.php');
}


if($_POST['action'] === 'logout') {
    logout(); 
}